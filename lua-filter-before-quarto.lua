local List = require 'pandoc.List'

function Div(el)
    if FORMAT == "latex" then
        -- environment is the first class
        local env = el.classes[1]
        if env == "doc" then
            -- passe l'attribut title dans options pour le filtre latex-environments de quarto
            for key, value in pairs(el.attributes) do
                if key == "title" then
                    el.attributes.options = value
                end
            end
            return el
        elseif el.attributes.tags == '["correction"]' then
            -- Pour les cellules de code exécutées avec le tag correction
            local data = List:new{}
            data:extend{pandoc.RawBlock('tex', "\\begin{correction}")}
            data:extend{el}
            data:extend{pandoc.RawBlock('tex', "\\end{correction}")}
            return data
        else
            return el
        end
    end
end

function Image(el)
    if FORMAT == "latex" then
        -- convert svg to pdf
        if string.sub(el.src, -4) == '.svg' then
            local pdfName = string.gsub(el.src, "svg", "pdf")
            pandoc.pipe('/usr/bin/inkscape', {el.src, '--export-filename', pdfName}, '')
            el.src = pdfName
            return el
        end
    end
end
