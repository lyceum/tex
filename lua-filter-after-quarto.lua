local List = require 'pandoc.List'

function Div(el)
    -- On rajoute un texte >>>sortie aux sorties de python non display
    -- TODO ne jamais afficher sortie si echo = False
    if el.classes:includes("cell-output") and not el.classes:includes("cell-output-display") then
        local data = List:new{}
        data:extend{pandoc.Para(pandoc.SmallCaps(">>sortie"))}
        data:extend{el}
        return data
    end
end

function Link(el)
    -- Clsse cite-source sur les liens
    if el.classes:includes("cite-source") then
        local data = List:new{}
        data:extend{pandoc.Strong(pandoc.SmallCaps("Source:")), pandoc.SoftBreak ()}
        data:extend{el}
        return pandoc.Emph(data)
    end
    return el
end