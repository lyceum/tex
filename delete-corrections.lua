function Div(el)
    -- Vide les div avec classe correction
    -- ::: correction
    if el.classes:includes ("correction") then
        return {}
    -- Vide les code de correction
    -- ::: {.cell tags="[\"correction\"]" execution_count="1"}
    elseif el.classes:includes("cell") and el.attributes.tags == "[\"correction\"]" then
        return {}
    end

    -- dans tous les autres cas renvoie la div «normale» générée par pandoc
    return el

end