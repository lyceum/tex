# tex

Tous les fichiers relatifs à latex.

## Réglages

- `mhchem`
- `si-unitx`

## Macro `\acompleter`

Pour ajouter des points jusqu'à la fin de la ligne.

## Macro \carreaux

Pour les réponses manuscrites des élèves avec carreaux Seyes.

- `\carreaux` 5 lignes par défaut
- `\carreaux[n]` n lignes

### Macro \papiermm

Pour les feuilles de papier millimetrées integrées.

- `\papiermm` 10 lignes par défaut
- `\papiermm[n]` n lignes

## pandoc

lua filters de pandoc comme un sub-tree.

        git subtree add --prefix pandoc-lua-filters git@github.com:pandoc/lua-filters.git master --squash

MàJ: `git subtree pull --prefix pandoc-lua-filters git@github.com:pandoc/lua-filters.git master --squash`

- include-files
- list-table tableaux puissants venant de `rst`.

## quarto extensions

- [quarto-ext/latex-environment](https://github.com/quarto-ext/latex-environment): émet des
  environnements ou commandes latex.

comme un sub-tree.

        git subtree add --prefix quarto-ext/latex-environment git@github.com:quarto-ext/latex-environment.git main --squash

MàJ: `git subtree pull --prefix quarto-ext/latex-environment git@github.com:quarto-ext/latex-environment.git main --squash`

- [quarto-ext/shinylive](https://github.com/quarto-ext/shinylive): émet des
  environnements ou commandes latex.

comme un sub-tree.

        git subtree add --prefix quarto-ext/shinylive git@github.com:quarto-ext/shinylive.git main --squash

MàJ: `git subtree pull --prefix quarto-ext/shinylive git@github.com:quarto-ext/shinylive.git main --squash`
